const config = {
    kafka_client_request_timeout: process.env['KAFKA_CLIENT_REQUEST_TIMEOUT'] || 60000,
    session_timeout: 15000,
    protocols: {
        roundrobin:'roundrobin',
        range:'range',
    },
    from_offset: {
        earliest:'earliest',
        none:'none',
        latest:'latest',
    },
    producer_config: {
        messages_count: process.env['KAFKA_PRODUCER_MESSAGES_COUNT'] || 200,
        interval_occurence: process.env['KAFKA_PRODUCER_INTERVAL_OCCURENCE'] || 5000
    }
};

module.exports = config;
