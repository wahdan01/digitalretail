const kafka = require('kafka-node');
const debug = require('debug')('digitalretail:kafka:producer');
const config = require('./config');
const _ = require('lodash');

let client;
let producer;
const { Producer } = kafka;

let messages = {};
let intervalSend;
const delimiter = '@#';

function whenProducerReady(url) {
  return new Promise((resolve, reject) => {
    if (!client) {
      debug('Generating new KAFKA Client');
      client = new kafka.KafkaClient({
        kafkaHost: url,
        requestTimeout: config.kafka_client_request_timeout,
      });
      kafka.KafkaClient.prototype.wrapTimeoutIfNeeded = function (socketId, correlationId, callback, overrideTimeout) {
        if (this.options.requestTimeout === false && overrideTimeout == null) {
          
          return callback;
        }
      
        const timeout = overrideTimeout || this.options.requestTimeout;
      
        let timeoutId = null;
      
        const wrappedFn = function () {
          clear();
          callback.apply(null, arguments);
        };
      
        function clear () {
          clearTimeout(timeoutId);
          timeoutId = null;
        }
      
        timeoutId = setTimeout(() => {
          let wasIncbQueue = this.unqueueCallback(socketId, correlationId);
          if (!wasIncbQueue) { return; }
          callback(new TimeoutError(`Request timed out after ${timeout}ms`));
          callback = _.noop;
        }, timeout);
      
        wrappedFn.timeoutId = timeoutId;
      
        return wrappedFn;
      };
      client.on('close', () => {
        debug('KAFKA Client connection closed');
        client = null;
        producer = null;
        if (intervalSend) {
          debug('Clearing interval send to kafka');
          clearInterval(intervalSend);
          intervalSend = null;
        }
      });
    }
    if (!producer) {
      debug('GENERATING FRESH KAFKA CLIENT AND KAFKA PRODUCER OBJECT');
      producer = new Producer(client);
      producer.on('ready', () => {
        debug('Producer is ready');
        resolve(producer);
      });
      producer.on('error', (err) => {
        debug('PRODUCER IS NOT READY');
        if (intervalSend) {
          debug('Clearing interval send to kafka');
          clearInterval(intervalSend);
          intervalSend = null;
        }
        reject(err);
      });
    } else {
      debug('INSIDE ELSE BLOCK OF whenProducerReady');
      resolve(producer);
    }
  });
}

function sendToKafka(url) {
  let payloads = [];
  _.forOwn(messages, (val, key) => {
    let keys = key.split(delimiter);
    payloads.push({
      topic: keys[0],
      messages: val,
      partition: keys[1],
    });
  });
  messages = {};
  return whenProducerReady(url)
    .then(producerObj => new Promise((resolve, reject) =>
      producerObj.send(payloads, (err, data) => {
        if (err) {
          debug('ERROR OCCURED WHILE SENDING OBJECT TO KAFKA');
          return reject(err);
        }
        debug('Data sent to producer');
        return resolve(data);
      })))
    .catch((error) => {
      debug('\t Exception occured in sendToKafka function', error);
      return Promise.reject(error);
    });
}

function sendToProducer(msg, partition, topic, url, immediate = false) {
  if (typeof intervalSend !== 'function') {
    debug('Initiated interval send to kafka');
    intervalSend = setInterval(() => {
      if (messages && getMessagesCount() > 0) {
        sendToKafka(url)
          .catch((error) => {
            debug('\t Exception occured in sendToProducer function', error);
            if (intervalSend) {
              debug('Clearing interval send to kafka');
              clearInterval(intervalSend);
              intervalSend = null;
            }
          });
      }
    }, config.producer_config.interval_occurence);
  }

  let key = topic + delimiter + partition;
  (messages[key] = messages[key] || []).push(JSON.stringify(msg));

  if (immediate || (messages && getMessagesCount() >= config.producer_config.messages_count)) {
    return sendToKafka(url);
  }
  else {
    return Promise.resolve();
  }
}

function getMessagesCount() {
  let sum = 0;
  _.forOwn(messages, (messageCollection) => {
    sum += messageCollection.length
  });
  return sum;
}


module.exports = {
  sendToProducer,
};
