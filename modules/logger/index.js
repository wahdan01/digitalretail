// const { kafkaProducer } = require('../kafka');
// const moment = require('moment');
// const debug =  require('debug')('digitalretail:logger:index')

// module.exports = class Logger {
//   constructor(market, enviromentName, topic, kafkaUrl) {
//     if (!!Logger.instance) {
//       debug(`Object Already Created`);
//       return Logger.instance;
//     }
//     Logger.instance = this;

//     debug(`construtucting logger object for url - ${kafkaUrl}`);
//     debug(`construtucting logger object for topic - ${topic}`);
//     this.topic = topic;
//     this.kafkaUrl = kafkaUrl;
//     this.partition = 0;
//     this.market = market;
//     this.enviromentName = enviromentName;
//     //instance name TBC
//     return this;
//   }

//   log({ log, type = "info" }) {
//     const logData = {};
//     logData.log = log;
//     logData.type = type;
//     logData.timeStamp = moment().format('YY-MM-DD-hh:mm:ss');
//     logData.enviromentName = this.enviromentName;
//     logData.requestId= this.requestId;
//     return kafkaProducer.sendToProducer(logData, this.partition, this.topic, this.kafkaUrl);
//   }

//   info(log){
//     this.log({log,type:'info'});
//   }
//   warn(log){
//     this.log({log,type:'warn'});
//   }
//   error(log){
//     this.log({log,type:'error'});
//   }

//   setRequestid(requestId){
//     this.requestId= requestId;
//   }
  
// }


module.exports ={
  KafkaLogger: require('./kafka')
}
