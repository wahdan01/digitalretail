

const Transport = require('winston-transport');
const { kafkaProducer } = require('../kafka');

  module.exports = class Kafka extends Transport {
    constructor(opts) {
      super(opts);
      this.topic = opts.topic;
      this.url = opts.url;
    }
    
    log(info, callback) {
      setImmediate(() => {
        this.emit('logged', info);
      });
      kafkaProducer.sendToProducer(info, 0, this.topic, this.url);
      callback();
    }
  };








